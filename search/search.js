//==============================================================================
//  Wordcloud
//==============================================================================

WordCloud(document.getElementById('wordcloud'), {
  list: tagwords,
  gridSize: 26,
  weightFactor: 20,
  fontFamily: getComputedStyle(document.body).getPropertyValue('--font-family-sans-serif'),
  rotateRatio: 0,
  color: function (word, weight) {
    var n = Math.floor(Math.random() * 4) + 1;
    switch(n) {
      case 1: return getComputedStyle(document.body).getPropertyValue('--primary');
      case 2: return getComputedStyle(document.body).getPropertyValue('--secondary');
      case 3: return getComputedStyle(document.body).getPropertyValue('--warning');
      default: return getComputedStyle(document.body).getPropertyValue('--dark');
    }
  },
  click: function(item) {
    var search_input = document.getElementById("search-input");
    search_input.value = item[0];
    search_input.dispatchEvent(new Event('keyup'));
  },
});

//==============================================================================
//  Search
//==============================================================================

SimpleJekyllSearch({
  searchInput: document.getElementById("search-input"),
  resultsContainer: document.getElementById("results-container"),
  json: "search.json",
  searchResultTemplate: "<a class='list-group-item list-group-item-action' href={url}><img class='float-left mr-3 border rounded bg-white' src='{url}card.svg' width='100' height='75' alt='{title}'><h5 class='text-left'>{title}</h5><p class='mb-0'>{description}</p></a>",
  noResultsText: "<div class='list-group-item list-group-item-danger'>Sorry. No results found! 😞</div>"
});
