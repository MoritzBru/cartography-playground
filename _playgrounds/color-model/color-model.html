---
title: "Color Model"
description: "Get to know the different color models in cartography field"
date: 2024-07-04
tags: Color Model
references:
  - author: "Liqiu Meng"
    title: "Geovisualisierung und Computergraphik:2D Graphik"
    date: 2021
  - author: "CorelDRAW"
    title: "Understanding Color Models"
    date: 2022
    link: http://product.corel.com/help/CorelDRAW/540240626/Main/CS/Doc/wwhelp/wwhimpl/js/html/wwhelp.htm?href=CorelDRAW-Understanding-color-models.html
  - author: "KRITA"
    title: "Color Models"
    link: https://docs.krita.org/zh_CN/general_concepts/colors/color_models.html
    date: 2024
  - author: "EasyRGB"
    title: "Color math and programming code examples"
    link: https://www.easyrgb.com/en/math.php
    date: 2024
  - author: "Wikimedia Commons"
    title: "SVG for different colors models"
    link: https://commons.wikimedia.org/wiki/Main_Page
    date: 2024
---
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- update 07.04.2024 Tang -->
  <style>
    .justified-text {
      text-align: justify;
    }
    .iframe-container {
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .center-table {
        display: flex;
        justify-content: center;
        align-items: center;
        margin: 20px 0;
    }
    table {
        border-collapse: collapse;
        width: 80%;
    }
    th, td {
        border: 1px solid black;
        padding: 8px;
        text-align: center;
    }
    th {
        background-color: #f2f2f2;
    }

    .left-align {
      text-align: left;
    }
  </style>
  <style>
    .zoom:hover {
        transform: scale(2);
        transition: transform 0.3s ease-in-out;
    }
  </style>
</head>

<div class="justified-text">
  <div class="col">
    <p>
      A color model is an abstract mathematical model used to describe and define colors. Each model defines colors using specific color components, and different color models are suitable for different applications. Colors play a crucial role in map design, and utilizing various color models allows us to achieve accurate and consistent color representation on various media and devices. This is essential for ensuring precise visual communication and aesthetic appeal in images.
    </p>
  </div>
</div>

<h2 class="mt-5">The main color models</h2>
<hr>

<p>
  This table shows the main color models could be used in cartography.
  <br>
  You can modify the values ​​of the indicators of different models in the fourth column to change the color.
</p>

<div class="center-table">
  <table>
    <tr>
      <th style="width: 400px;">Color Model</th>
      <th style="width: 1000px;">Model Description</th>
      <th style="width: 800px;">Model Display</th>
      <th>Examples</th>
      <th style="width:600px;">Application Areas</th>
      <th style="width: 800px;">Advantages</th>
    </tr>
    <tr>
      <td>RGB</td>
      <td class="left-align">Uses red (R), green (G), and blue (B) components to define the amount of red, green, and blue light in the color. In a 24-bit image, each component is represented as a number between 0 and 255.</td>
      <!-- copyright: https://commons.wikimedia.org/wiki/File:RGB_color_model.svg -->
      <td><img src="imgs/RGB_color_model.svg" class="img-fluid mx-auto d-block zoom" alt="SVG Image"></td>
      <td>
        <iframe src="imgs/RGB.html" style="width:130px"></iframe>
      </td>
      <td class="left-align">Digital maps</td>
      <td class="left-align">- Stores and displays a wide range of colors<br>- Easy to operate and adjust on computer</td>
    </tr>
    <tr>
      <td>CMYK</td>
      <td class="left-align">Uses cyan (C), magenta (M), yellow (Y), and black (K) components to define the color. Each component ranges from 0 to 100 and is expressed as percentages.</td>
      <!-- copyright: https://commons.wikimedia.org/wiki/File:CMYK_color_model.svg -->
      <td>
        <img src="imgs/CMYK_color_model.svg" class="img-fluid mx-auto d-block zoom" alt="SVG Image">
      </td>
      <td .iframe-container>
        <iframe src="imgs/CMYK.html" style="width:130px"></iframe>
      </td>
      <td class="left-align">Printed maps</td>
      <td class="left-align">- Precise color reproduction<br>- Suitable for mass printing and publishing</td>
    </tr>
    <tr>
      <td>HSV</td>
      <td class="left-align">Uses hue (H), saturation (S), and value (V)components to define the color.
        <p>
          Hue: wavelength of light on the color wheel [0°, 360°]. <br>
          Saturation: purity of a color between 0% in the center and 100% at the edge. <br>
          Value: between 0% (black) and 100% (white)
        </p>
      </td>
      <!-- copyright: https://upload.wikimedia.org/wikipedia/commons/a/a0/Hsl-hsv_models.svg -->
      <td><img src="imgs/Hsl-hsv_models.svg" class="img-fluid mx-auto d-block zoom" alt="SVG Image"></td>
      <td>
        <iframe src="imgs/HSV.html" style="width:130px"></iframe>
      </td>
      <td class="left-align">Map design<br>Color selection</td>
      <td class="left-align">Enhances map legibility and visual aesthetics</td>
    </tr>
    <tr>
      <td>Lab</td>
      <td class="left-align">Based on human visual perception, including brightness (Luminance:0% to 100%), green-red (a-axis: -128 to 127), and blue-yellow (b-axis: -128 to 127 )</td>
      <!-- copyright: https://upload.wikimedia.org/wikipedia/commons/a/a0/Hsl-hsv_models.svg -->
      <td><img src="imgs/The_principle_of_the_CIELAB_colour_space.svg" class="img-fluid mx-auto d-block zoom" alt="SVG Image"></td>
      <td>
        <iframe src="imgs/Lab.html" style="width:130px"></iframe>
      </td>
      <td class="left-align">High-precision color conversion</td>
      <td class="left-align">Ensures color consistency across different devices and media</td>
    </tr>


  </table>
</div>


<h2 class="mt-5">Hands-On</h2>
<hr>
<h4 class="mt-5">Screen Color Picking & Color Model Conversion</h4>

<p>
  You can start interacting with the color model by clicking on the square below
  <br>
  Click the eyedropper and move the mouse to any position on the screen to pick up a color.
  <br>
  Drag the color axis to switch colors.
</p>
<div class="iframe-container">
  <iframe 
  src="imgs/Hands-on.html"
  width="800" 
  height="400" 
  frameborder="0" 
  style="border:0" 
  allowfullscreen>This page does not support iFrames.
</iframe>
</div> 
